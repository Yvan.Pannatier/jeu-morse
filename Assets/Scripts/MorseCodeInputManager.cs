using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MorseCodeInputManager : MonoBehaviour
{
    // GUI
    public TextMeshProUGUI hintText;
    public TextMeshProUGUI userAnswerText;

    public Slider progressBar;

    // Time unit
    public float symbolDuration = 0.1f; 
    
    // Duration of all morse symbols
    private float dotDuration;
    private float dashDuration;
    private float letterPause; 
    private float wordPause;


    private bool isRecordingInput = false;
    private bool isKeyPressed = false;
    private string currentMorseCode = "";
    private string finalMorseCode = "";
    private float startTime; 
    private float startSymbolTime;
    
    void Start(){
        dotDuration = symbolDuration;
        dashDuration = 3*symbolDuration;
        letterPause = 3*symbolDuration;
        wordPause = 7*symbolDuration;
        hintText.text = "Waiting";
    }
    void Update()
    {
        HandleUserInput();
        UpdateHint();
        HandleEnd();
        userAnswerText.text = finalMorseCode;
    }

    void ProcessCurrentSymbol()
    {
        float elapsedTime = Time.timeSinceLevelLoad - startSymbolTime;
        if (elapsedTime < dotDuration)
        {
            currentMorseCode += ".";
        }
        else if (elapsedTime < dashDuration)
        {
            currentMorseCode += "-";
        }
        else
        {
            isKeyPressed = false;
            isRecordingInput = false;
        }

        startTime = Time.timeSinceLevelLoad; 
    }

    void HandleUserInput() {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!isRecordingInput)
            {
                isRecordingInput = true;
                currentMorseCode = ""; 
            }

            float pauseDuration = Time.timeSinceLevelLoad - startTime;
            if(pauseDuration > letterPause && pauseDuration <= wordPause) {
                // Use double space to mark new word
                currentMorseCode += "  ";
                
            } else  if(pauseDuration > symbolDuration && pauseDuration <= letterPause) {
                // Use space to mark new letter
                currentMorseCode += " ";
            }

            startTime = Time.timeSinceLevelLoad;
            startSymbolTime = Time.timeSinceLevelLoad;
            isKeyPressed = true;

        } else if (Input.GetKeyUp(KeyCode.Space) && isKeyPressed) {
            ProcessCurrentSymbol();
            isKeyPressed = false;
        }
    }

    void HandleEnd() {
        if(isRecordingInput && !isKeyPressed) {
            float pauseDuration = Time.timeSinceLevelLoad - startTime;

            if (pauseDuration > wordPause) {
                isRecordingInput = false;
                finalMorseCode = currentMorseCode;
                startTime = Time.timeSinceLevelLoad;
                return;
            }
        }
    }

    void UpdateHint() {
        if(isRecordingInput) {
            float holdDuration = Time.timeSinceLevelLoad - startTime;
            float progress = Mathf.Clamp(holdDuration / wordPause, 0.0f, 1.0f);
            progressBar.value = progress;

            if(isKeyPressed) {
                if(progressBar.value < dotDuration / wordPause) {
                    progressBar.fillRect.GetComponent<Image>().color = Color.blue;
                    hintText.text = "Dot";
                } else  if(progressBar.value < dashDuration / wordPause) {
                    progressBar.fillRect.GetComponent<Image>().color = Color.green;
                    hintText.text = "Dash";
                } else {
                    progressBar.fillRect.GetComponent<Image>().color = Color.red;
                    hintText.text = "Error: Release Key and restart!!";
                }
            } else {
                if(progressBar.value < symbolDuration / wordPause) {
                    progressBar.fillRect.GetComponent<Image>().color = Color.blue;
                    hintText.text = "Symbole Pause";
                } else  if(progressBar.value < letterPause / wordPause) {
                    progressBar.fillRect.GetComponent<Image>().color = Color.green;
                    hintText.text = "Letter Pause"; }
                else  if(progressBar.value < wordPause / wordPause) {
                    progressBar.fillRect.GetComponent<Image>().color = Color.green;
                    hintText.text = "Word Pause";
                } else {
                    progressBar.fillRect.GetComponent<Image>().color = Color.red;
                    hintText.text = "End of Transmission";
                }
            }
            
        } else {
            progressBar.value = 0.0f;
            hintText.text = "Waiting";
            currentMorseCode = "";
        }
    }
}
