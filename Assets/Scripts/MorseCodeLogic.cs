using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MorseCodeLogic : MonoBehaviour
{

    public float timeUnit = 0.1f;

    // GUI
    public TextMeshProUGUI morseCodeText;
    public TMP_InputField userInputField;
    public Button translateButton;
    public Button playAudioSignalButton;
    public Button playVisualSignalButton;


    // LAMP 
    public Image lampSprite;
    public Color lightOnColor;
    public Color lightOffColor;


    //AUDIO 
    public AudioSource morseCodeAudio;
    public int sampleRate = 44100;
    public float frequency = 440.0f;
    private AudioClip dotSound;
    private AudioClip dashSound;


    // Morse-English Dictionaries
    public Dictionary<string, char> morseToEnglish = new Dictionary<string, char>()
    {
        {".-", 'A'},{"-...", 'B'},{"-.-.", 'C'},{"-..", 'D'},{".", 'E'},
        {"..-.", 'F'}, { "--.", 'G'}, {"....", 'H'}, { "..", 'I'}, { ".---", 'J'},
        { "-.-", 'K'}, { ".-..", 'L'}, { "--", 'M'}, { "-.", 'N'}, { "---", 'O'},
        { ".--.", 'P'}, { "--.-", 'Q'}, { ".-.", 'R'}, { "...", 'S'}, { "-", 'T'},
        { "..-", 'U'}, { "...-", 'V'}, { ".--", 'W'}, { "-..-", 'X'}, { "-.--", 'Y'},
        { "--..", 'Z'},
        { "-----", '0'}, { ".----", '1'}, { "..---", '2'}, { "...--", '3'},
        { "....-", '4'}, { ".....", '5'}, { "-....", '6'}, { "--...", '7'}, { "---..", '8'},
        { "----.", '9'}, { ".-.-.-", '.'}, { "--..--", ','}, { "---...", ':'}, { "----..", '?'},
        { "-....-", '-' }, {".--.-.-", '('}, {"-..-.-", ')'}, {"-..-.",'/'}, {" ", ' '}
    };
    public Dictionary<char, string> englishToMorse = new Dictionary<char, string>()
    {
        {'A', ".-"}, {'B', "-..."}, {'C', "-.-."}, {'D', "-.."}, {'E', "."},
        {'F', "..-."}, {'G', "--."}, {'H', "...."}, {'I', ".."}, {'J', ".---"},
        {'K', "-.-"}, {'L', ".-.."}, {'M', "--"}, {'N', "-."}, {'O', "---"},
        {'P', ".--."}, {'Q', "--.-"}, {'R', ".-."}, {'S', "..."}, {'T', "-"},
        {'U', "..-"}, {'V', "...-"}, {'W', ".--"}, {'X', "-..-"}, {'Y', "-.--"},
        {'Z', "--.."}, {'0', "-----"}, {'1', ".----"}, {'2', "..---"}, {'3', "...--"},
        {'4', "....-"}, {'5', "....."}, {'6', "-...."}, {'7', "--..."}, {'8', "---.."},
        {'9', "----."}, {'.', ".-.-.-"}, {',', "--..--"}, {':', "---..."}, {'?', "----.."},
        {'-', "-....-"}, {'(',".--.-.-"}, {')',"-..-.-"}, {'/', "-..-."}
    };



    public void Start()
    {
        lampSprite.GetComponent<Image>().color = lightOffColor;
        CreateDotSound();
        CreateDashSound();
    }

    

    public string GenerateRandomMorseCode()
    {
        string morseCodeMessage = "";
        int numLetters = Random.Range(1, 6);

        for (int i = 0; i < numLetters; i++)
        {
            char randomLetter = (char)Random.Range(65, 91);
            string morseCode = englishToMorse.ContainsKey(randomLetter) ? englishToMorse[randomLetter] : "";
            morseCodeMessage += morseCode + " ";
        }

        return morseCodeMessage.Trim();
    }

    public string TranslateMorseCode(string morseCodeMessage)
    {
        string englishText = "";
        string[] morseCodeWords = morseCodeMessage.Split(' ');

        foreach (string morseCode in morseCodeWords)
        {
            if (morseToEnglish.ContainsKey(morseCode))
            {
                englishText += morseToEnglish[morseCode];
            }
            else
            {
                englishText += "?";
            }
        }

        return englishText;
    }

    public string ToMorse(string message) {
        string morseCodeMessage = "";
        message = message.ToUpper();
        foreach(char letter in message) {
            string morseCode = englishToMorse.ContainsKey(letter) ? englishToMorse[letter] : "";
            morseCodeMessage += morseCode + " ";
        }
        return morseCodeMessage;
    }

    public void OnTranslateButtonClick()
    {
        string text = ToMorse(userInputField.text);
        morseCodeText.text = text;
    }

    public void OnPlayAudioSignalButtonClick() {
        string message = morseCodeText.text;
        PlayAudioMorseCode(message);
    }

    public void OnPlayVisualSignalButtonClick() {
        string message = morseCodeText.text;
        PlayVisualMorseCode(message);
    }

    public void PlayVisualMorseCode(string message)
    {
        StartCoroutine(LightBlink(message));
    }

    public void PlayAudioMorseCode(string message)
    {
        StartCoroutine(AudioBeep(message));
    }

    IEnumerator LightBlink(string message)
    {
        foreach(char symbol in message)
        {
            lampSprite.color = lightOnColor;
            if (symbol == '.')
            {
                yield return new WaitForSeconds(timeUnit);
            }
            else if (symbol == '-')
            {
                yield return new WaitForSeconds(3* timeUnit);
            }
            lampSprite.color = lightOffColor;
            if(symbol == ' ')
            {
                print("nice");
                yield return new WaitForSeconds(3*timeUnit);
            } else
            {
                yield return new WaitForSeconds(timeUnit);
            }
           
        }       
    }

    IEnumerator AudioBeep(string message)
    {
        foreach (char symbol in message)
        {
            if (symbol == '.')
            {
                morseCodeAudio.clip = dotSound;
                morseCodeAudio.Play();
                yield return new WaitForSeconds(timeUnit);
            }
            else if (symbol == '-')
            {
                morseCodeAudio.clip = dashSound;
                morseCodeAudio.Play();
                yield return new WaitForSeconds(3 * timeUnit);
            }
            if (symbol == ' ')
            {
                print("nice");
                yield return new WaitForSeconds(3 * timeUnit);
            }
            else
            {
                yield return new WaitForSeconds(timeUnit);
            }
        }
    }

    private void CreateDotSound()
    {
        int samples = (int)(timeUnit * sampleRate);
        float[] audioData = CreateAudioData(samples);
        dotSound = AudioClip.Create("DotSound", samples, 1, sampleRate, false);
        dotSound.SetData(audioData, 0);
    }

    private void CreateDashSound()
    {
        int samples = (int)(3 * timeUnit * sampleRate);
        float[] audioData = CreateAudioData(samples);
        dashSound = AudioClip.Create("DashSound", samples, 1, sampleRate, false);
        dashSound.SetData(audioData, 0);
    }

    private float[] CreateAudioData(int samples)
    {
        float[] audioData = new float[samples];
        for (int i = 0; i < samples; ++i)
        {
            audioData[i] = Mathf.Sin(2 * Mathf.PI * frequency * i / sampleRate);
        }
        return audioData;
    }
}